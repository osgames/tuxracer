tux_course_light 0 -on -position { 1 1 0 0 } -diffuse { 0.4 0.4 0.4 1 } \
     -specular { 0.0 0.0 0.0 1 } -ambient { 0.8 0.8 0.8 1.0 }

# This light is to get a nice highlight on tux
tux_course_light 1 -on -position { 1 1 2 0 } -specular { 0.8 0.8 0.8 1 } 
